import { schemaForCreate,schemaForUpdate } from "../modules/user/user.scemas.mjs";

// Middleware to validate user before create
export const validateBeforeCreateMW = async (req, res , next )=> {
    await schemaForCreate.validateAsync(req.body);
    next();
}

// Middleware to validate user before update
export const validateBeforeUpdateMW = async (req, res , next )=> {
    await schemaForUpdate.validateAsync(req.body);
    next();
}
