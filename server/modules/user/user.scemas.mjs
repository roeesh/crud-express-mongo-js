import Joi from "joi";

export const schemaForCreate = Joi.object({
    first_name: Joi.string()
        .min(2  )
        .max(10)
        .required(),

    last_name: Joi.string()
        .min(3)
        .max(10)
        .required(),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
        .required(),
    
    phone: Joi.string()
        .pattern(new RegExp('^[0-9]{10}$'))
        .required()
});

export const schemaForUpdate = Joi.object({
    first_name: Joi.string()
        .min(3)
        .max(10),

    last_name: Joi.string()
        .min(3)
        .max(10),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
    
    phone: Joi.string()
        .pattern(new RegExp('^[0-9]{10}$'))
});

